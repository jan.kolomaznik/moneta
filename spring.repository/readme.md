[remark]:<class>(center, middle)
# Spring ORM: `Repository`
## Java Persistence API


[remark]:<slide>(new)
## Spring DATA-JPA
Java Persistence API poskytuje vývojářům objektově / relační mapování a zařízení pro správu relačních dat v Java aplikacích. 
- Jedná se o "nadstavbu" nad **JDBC**. 

Spring ve výchozím nastavení používá Hibernate v této vrstě.

Spring Data umožňuje zcela odstranit implementace DAO a použití EntityManageru. Rozhraní DAO je nyní jediným prvkem, který musíme explicitně definovat.

Spring poskutuje pomocné třídy a rozhraní obsahující základní CRUD operace, které můžeme rozšířit.

Pro realizaci práce s datbází tedy musíme:
- Definovat Entity
- Vytvořit DAO rozhrnaní
- Nakofigurovat připojení

[remark]:<slide>(new)
## Entities

Entita je Java POJO objekt s persistencí do databáze. 

Typicky jedna Entita představuje jednu tabulku v relační databázi, a každá instance objektu odpovídá jednomu řádku v této tabulce.

Stav entity je reprezentována buď prostřednictvím atributů objektu, nebo vazbami na jiné entity.

Tato atributy nebo vazby jsou mapovány pomocí annotací.

Při jejich definování je dobé se držet JPA API (Annotace z balíčku `javax.persistence.*`). 
- Protože spring používí Hibernat, jsou dostupné i annotace `org.hibernate.*`
- Tyto dva druhy annotací dělají v podstatě to stejné
- **Není dobré je míchat dohromady**

[remark]:<slide>(new)
### Požadavky na Entitní třídu

- Třída musí být označená s anotací `@Entity`.

- Třída musí mít veřejný (`public`) či chráněné (`protected`) konstruktor bez argumentů, může mít jiné konstruktory.

- Třída nesmí být označena za `final`, stejně tak metody nebo atributy.

- Entity používané v session beans musí implementovat rozhraní `Serializable`.

- Entity mohou být potomky a být děděny ne-entitními.

- Atributy třídy mohou být `private`, `protected` nebo `package-protected`. Klienti musí přistupovat k atributům pouze prostřednictvím metod.

[remark]:<slide>(new)
### Povolené datové typy v Entitní třídě

- Java primitive types

- java.lang.String

- Other serializable types: 
  * Wrappers of Java primitive types,
  * `java.math.BigInteger`, `java.math.BigDecimal`,
  * `java.util.Date`, `java.util.Calendar`, `java.sql.Date`, `java.sql.Time`, `java.sql.TimeStamp`, 
  * User-defined serializable types, 
  * `byte[]`, `Byte[]`, `char[]`, `Character[]`.

- Enumerated types

- Other entities and/or collections of entities

- Embeddable classes

[remark]:<slide>(new)
### Annotace pro definici sloupců v DB
Pro atributy třídy můžeme použít následující annotace:

| Annotation | Description |
| :--- | :--- |
| `@Id` | Definice id v databází. |
| `@GeneratedValue` | Definice generování id v db |
| `@Table` | Definice názvu tabulky v databází. |
| `@Embedded` | Určuje mapování na skupinu sloupců podle třídy |
| `@Transient` | Zabraňuje uložení atributu do databáze |
| `@Column` | Nastavení vlastnotí sloupců. |
| `@UniqueConstraint` | Definice UniqueConstraint |
| `@ColumnResult` | Tato anotace odkazuje na název sloupce v dotazu SQL pomocí klauzule select. |


[remark]:<slide>(new)
#### Příklad Entity
```java
@Entity
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(columnDefinition = "VARCHAR(40)", 
            unique = true, 
            nullable=false)
    private String name;
    
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @Embedded
    private Audit audit;
    
    ...
    // Setters/getters
}
```

*Definice Třídy `Audit` následuje záhy.*

[remark]:<slide>(new)
## Vytváření DAO rozhraní
#### Příklad ...

[remark]:<slide>(new)
## Konfigurace
Spring boot minimalizuje potřebnou konfiguraci.

Pokud použijeme v dependecíes jednu databázi, pozná to připojí se k ní.

#### Příklad připojení H2 in memory DB
```groovy
dependencies {
    // Spring boot
    compile 'org.springframework.boot:spring-boot-starter-data-jpa'
    // Database
    runtime 'com.h2database:h2'
```

[remark]:<slide>(wait)
```yaml
spring:
  h2:
    console:
      enabled: true

  datasource:
    url: jdbc:h2:file:./db/main-database;DB_CLOSE_ON_EXIT=FALSE
    username: test
    password: test
    driverClassName: org.h2.Driver
  jpa:
    show-sql: false
```

[remark]:<slide>(new)
[remark]:<class>(center, middle)
## Pokročilé mapování entit

[remark]:<slide>(new)
### Perzistence kolekcí

Entity mohou obsahovat kolekce jiných entit.

Pro kolekce mohou týt použitý následující rozhraní: *java.util.Collection, java.util.Set, java.util.List, java.util.Map*

Pokud kolekce obsahuje primitivní typy, musá být označena anotací `@ElementCollection` s atibutem `fetch` udává `LAZY` nebo `EAGER`.

```java
@Entity
public class Person {
 
    ...
    
    @ElementCollection(fetch=EAGER)
    protected Set<String> nickname = new HashSet<>();
    
    ...
}
```

[remark]:<slide>(new)
### Zivotní cyklus Entit
Pomocí anotací `@PrePersist`, `@PreUpdate` a `@PreRemove` lze určit okamžik validace během životního cyklu Entity.

[remark]:<slide>(wait)
#### Příklad nastavení času vytvoření
```java
@Embeddable
public class Audit {
 
    @Column(name = "created_on")
    private LocalDateTime createdOn;
     
    @Column(name = "updated_on")
    private LocalDateTime updatedOn;
    
    @PrePersist
    public void prePersist() {
        createdOn = LocalDateTime.now();
    }
 
    @PreUpdate
    public void preUpdate() {
        updatedOn = LocalDateTime.now();
    }
 
    //Getters and setters
}
```

[remark]:<slide>(new)
### Validace persistentních atributů 

Pro validaci se používá API JavaBeans Validation.



Například použití annotace `@NotNull`

```java
@Entity
public class Contact implements Serializable {
        
    @NotNull
    protected String name;
    ...
    
```

[remark]:<slide>(wait)
Validace pomocí regulárního výrazu `@Pattern` a validace nastavení datumu.

```java
    @Pattern(regexp = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
            message = "{invalid.phonenumber}")
    protected String mobilePhone;
    
    @Temporal(TemporalType.DATE)
    @Past
    protected Date birthday;
    ...
}
```

[remark]:<slide>(new)
### Primární klíče (jednoduché)

Jednoduché primární klíče se označují anotaci `@Id`.

Jako primární klíč lze použit: 
- celočíselné primitivní typy, 
- Java primitive wrapper types, 
- `java.lang.String`, 
- `java.util.Date`, `java.sql.Date`, 
- `java.math.BigDecimal`, `java.math.BigInteger`
- `java.util.UUID`

U některých typů klíčů lze nastavit strategii pro generování

```java
@Entity
public class Contact implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    ...
}
```

[remark]:<slide>(new)
### Primární klíče (složené)

Složené primární klíče jsou reprezentovány třídou, která:

* Musí být public.
* Gettry a Settry musí mít viditelnost **větší než** `private`.
* Musí mít *default* konstruktor.
* Musí implementovat metody `hashCode()` a `equals(Object other)`.
* Musí implementovat rozhraní `Serializable`.
* Složený klíč musí výt prezentován více atributy.

[remark]:<slide>(new)
### Primární klíče (složené)

```java
public final class LineItemKey implements Serializable {
    
    private Integer customerOrder;
    private Integer itemId;

    /* Getters and setters, hashcode a equals */
    ...
    
```

[remark]:<slide>(wait)
Použití v Entitě:
```java
@Entity
public LineItem {
    
    @EmbeddedId
    private LineItemKey lineItemKey;
    
    /* Getters and setters */
    ...
}
```

[remark]:<slide>(new)
## Vztahy mezi třídami

Směr vztahu může být obousměrný, nebo jednosměrný.

Vlastnící strana vztahu určuje, jakým způsobem je prováděna aktualizace vztahů v databázi.

U vazeb lze rovněž nastavit kaskádní operaci definovanou ve třídě `javax.persistence.CascadeType`: 

*ALL, DETACH, MERGE, PERSIST, REFRESH, REMOVE* 

```java
@OneToMany(cascade=REMOVE, mappedBy="customer")
private Set<Order> orders = new HashSet();
``` 

[remark]:<slide>(new)
### One-to-one

Každá instance entity se vztahuje k jedné instanci jiného subjektu. 

Například: sklad, v němž každý skladovací zásobník obsahuje jeden ovládací prvek

Používá se anotace `@OneToOne` na odpovídajícím atributu.

![](media/1-1_FK.png)

[remark]:<slide>(new)
#### Příklad One-to-one
```java
@Entity
@Table(name = "users")
public class User {
     
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;
 
    // ... getters and setters
}
```

```java
@Entity
@Table(name = "address")
public class Address {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    @OneToOne(mappedBy = "address")
    private User user;
 
    //... getters and setters
}
```


[remark]:<slide>(new)
### One-to-many

Instance entita může souviset s více instancí jiných subjektů. 

Například: Prodejní objednávky může mít více řádkových položek.

Používá se anotace `@OneToMany` na odpovídajícím atributu.

[remark]:<slide>(wait)
### Many-to-one

Více instancí entity může být vztažena k jedné instanci jiného subjektu. 

V příkladu právě výše, vztah řádkové položky k prodejní objednávce. 

Používá se anotace `@ManyToOne` na odpovídajícím atributu.

[remark]:<slide>(new)
### Many-to-many

Instance entity může být spojena s více instancí jiných entit. 

Například: každý kurz má mnoho studentů, a každý student může trvat několik kurzů.

Používá se anotace `@ManyToMany` na odpovídajícím atributu.


[remark]:<slide>(new)
### Dědičnost

Entity podporují třídní dědičnost, polymorfizmus. 

Entit třídy mohou dědit z non-entitnách tříd, a naopak. Entitní třídy mohou bát i abstraktní.

```java
@Entity
public abstract class Employee {
    @Id
    protected Integer employeeId;
    ...
}
```

```java
@Entity
public class FullTimeEmployee extends Employee {
    protected Integer salary;
    ...
}
```

[remark]:<slide>(new)
### Dědičnost (strategie mapování)

Dědičnost je možné da relační databáze namapovat třemi způsoby:

1. Strategie *A single table per class hierarchy*
2. Strategie *A table per concrete entity class*
3. Strategie *A "join" strategy*
