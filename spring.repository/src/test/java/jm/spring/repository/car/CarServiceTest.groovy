package jm.spring.repository.car

import jm.spring.repository.car.trip.Trip
import spock.lang.Specification
import spock.lang.Unroll

class CarServiceTest extends Specification {

    private CarService carService;
    private CarRepository carRepository;

    public void setup() {
        carService = new CarService();
        carService.carRepository = carRepository = Mock(CarRepository)
    }

    public void "Find Car by registration simple test."() {
        given:
        String reg = "ABC-12340";
        List<Trip> trips = [new Trip(), new Trip()]

        when:
        Car car = carService.create(reg, trips);

        then:
        car != null;
        car.getRegistration() == reg;
        
    }

    @Unroll
    def "Validace registračni zmanky #reg == #valid"() {
        expect:
        valid == carService.validRegistration(reg)

        where:
        reg        | valid
        "ASDFGH"   | true
        "ASDFGHJ"  | false
        "ASDFGHJK" | false
    }



    def findAll() {
    }
}
