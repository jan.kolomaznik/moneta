package jm.spring.repository;

import jm.spring.repository.car.Car;
import jm.spring.repository.car.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Optional;

@ShellComponent
@SpringBootApplication
public class RepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepositoryApplication.class, args);
    }

    @Autowired
    private CarRepository carRepository;



    @ShellMethod("Create car")
    public Car createCar(String registration) {
        Car car = new Car();
        car.setRegistration(registration);
        return carRepository.save(car);
    }

    @ShellMethod("Print all cars")
    public void printCar(String direction) {
        Pageable pageable = PageRequest
                .of(0,5, Sort.by(
                        Sort.Direction.valueOf(direction), "registration"));

        carRepository
                .findAll(pageable)
                .forEach(System.out::println);
    }

    @ShellMethod("Find by registration")
    public Optional<Car> findByRegistraion(String registation) {
        return carRepository.findByRegistration(registation);
    }
}
