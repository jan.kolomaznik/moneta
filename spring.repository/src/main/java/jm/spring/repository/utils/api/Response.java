package jm.spring.repository.utils.api;

import lombok.Data;
import lombok.Getter;

@Data
public class Response<T> {

    public static <P> Response<P> of(P payload){
        return new Response<>(payload);
    }

    private final T payload;

    Response(T payload) {
        this.payload = payload;
    }
}
