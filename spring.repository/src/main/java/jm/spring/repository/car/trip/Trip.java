package jm.spring.repository.car.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jm.spring.repository.car.Car;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Trip {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToOne
    @JsonIgnore
    private Car car;

}
