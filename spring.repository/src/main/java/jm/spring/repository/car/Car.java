package jm.spring.repository.car;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jm.spring.repository.car.trip.Trip;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Car {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String registration;

    @JsonIgnore
    @OneToMany(mappedBy = "car")
    private List<Trip> trips;
}
