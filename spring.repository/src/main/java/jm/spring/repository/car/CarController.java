package jm.spring.repository.car;

import io.swagger.annotations.*;
import jm.spring.repository.utils.api.Response;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Api(tags = "/api/car")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping("/car")
    public Page<Car> getCars(
            @RequestParam(defaultValue = "0") int page,
            @Validated @RequestParam(required = false, defaultValue = "100") @Max(1000) int size
    ) {
        return carService.findAll(PageRequest.of(page, size));
    }

    @Data
    @ApiModel("Create car request DTO")
    static class PostCarRequest {

        //@Pattern(regexp = "[A-Z0-1]{7}")
        @ApiModelProperty(
                example = "ABC-1259",
                notes = "Registrační značka auta",
                required = true)
        private String registration;
    }

    @PostMapping(
            path = "/car",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Response<Car> postCar(
            @Validated @RequestBody PostCarRequest request)
    {
        Car car = carService.create(request.getRegistration(), null);
        return Response.of(car);
    }

    @GetMapping("/car/{id}")
    public Car getCarByID() {
        return new Car();
    }

    @PutMapping("/car/{id}")
    public Car putCarByID() {
        return new Car();
    }

    @DeleteMapping("/car/{id}")
    public void deleteCarByID() {
    }
}
