package jm.spring.repository.car;

import jm.spring.repository.car.trip.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CarService {

     @Autowired
     private CarRepository carRepository;

    public Optional<Car> findByRegistration(String registration) {
        return carRepository.findByRegistration(registration);
    }

    public Page<Car> findAll(Pageable pageable) {
        return carRepository.findAll(pageable);
    }

    public Car create(String registration, List<Trip> tripList) {
        Car car = new Car();
        car.setRegistration(registration);
        car.setTrips(tripList);
        return carRepository.save(car);
    }

    public Car save(Car car) {
        return carRepository.save(car);
    }

    public  Optional<Car> findById(Long aLong) {
        return carRepository.findById(aLong);
    }

    public void deleteById(Long aLong) {
        carRepository.deleteById(aLong);
    }

    private boolean validRegistration(String reg) {
        return reg.length() == 7;
    }
}
