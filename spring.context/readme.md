[remark]:<class>(center, middle)
# Spring context

[remark]:<slide>(new)
## Typy Injekce pomocí
Spring podporuje tři typy **injekce**.
  - Property inject
  - Constructor inject
  - Setter inject
  
[remark]:<slide>(wait)
### Property inject
Framework si sám najde danou property pomocí reflexe.

```java
@Autowired
private CarDao carDao;
```

[remark]:<slide>(new)
### Constructor inject
Při vytváření pošle přes konstruktor instance potřebných component.

```java
private CarDao carDao;

@Autowired
public CarServiceImpl(CarDao carDao) {
        this.carDao = carDao;
}
```

[remark]:<slide>(wait)
### Setter inject
Při vytváření je nahrána instance pomocí setter-u.

```java
private CarDao carDao;

@Autowired
public void setCarDao(CarDao carDao) {
        this.carDao = carDao;
}
```

[remark]:<slide>(new)
**Spring podporuje několik způsobů konfigurace**:
  - XML (legacy)
  - Anotacemi
  - Definicí v kodu (metodou)

[remark]:<slide>(new)
## Dependency injection (DI)
Tento návrhový vzor souvisí přímo s IoC. 

Jedná se o mechanismus, kdy je do naší třídy vložena (injektována) instance jiné třídy. 

O tuto injekci se stará sám Framework podle konfigurace. 

Pro přehlednost a větší flexibilitu je dobré mít oddělenu konfiguraci od implementace. 

Existují tři typy injekce:



[remark]:<slide>(new)
#### Příklad Java konfigurace

```java
// jedná se o konfiguraci
@Configuration
// naimportuje konfiguraci z třídy StorageConfig
@Import({StorageConfig.class})
// skenuje cz.itnetwork a tvoří beany (@Component, @Service...)
@ComponentScan("cz.itnetwork")
public class ContextConfig {
        // vytvoří beanu typu CarDao a názvem carRepository
        @Bean(name="carRepository")
        public CarDao carDao() {
                return new CarDaoImpl();
        }

        // vytvoří beanu CarService a injektuje ji CarDao (CarRepository)
        @Bean
        @Autowired
        public CarService carService(CarDao carDao) {
                return new CarServiceImpl(carDao);
        }
}
```

[remark]:<slide>(new)
### XML konfigurace
Je reprezentována XML souborem. 

Konfigurace se musí nacházet v Resources a být na classpath.

Jedná se o starší způsob z "původniho".

Trendem je se tomuto způsobu konfigurace vyhnou, ale přesto se s ním čato setkáte.

*Doporučení*: naučit se přepsat xml do Java

```xml
<bean id="..." class="..."> vytvoří beanu
<import resource="..."/> import jiné konfigurace
<context:component-scan base-package="..." /> skenování package
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="..." class="...">   
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <bean id="..." class="...">
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions go here -->

</beans>
```


