[remark]:<class>(center, middle)
# Spring Boot
## JDBC Template

[remark]:<slide>(new)
## Uvod
Všechny třídy na Spring JDBC jsou rozděleny do čtyř samostatných balíčků:

- **core** - základní funkce JDBC. 
  Některé důležité třídy v rámci tohoto balíčku zahrnují `JdbcTemplate`, `SimpleJdbcInsert`, `SimpleJdbcCall` a `NamedParameterJdbcTemplate`.
  
- **datasource** - třídy nástrojů pro přístup k datovému zdroji. 
  Má také různé implementace datových zdrojů pro testování kódu JDBC mimo kontejner Java EE.

- **objekt** - přístup DB objektově orientovaným způsobem. Umožňuje provádět dotazy a vracet výsledky jako obchodní objekt. 
  Také mapuje výsledky dotazů mezi sloupci a vlastnostmi obchodních objektů.
  
- **support** - podporují třídy pro třídy v jádrových a objektových balíčcích. 
  Např. poskytuje funkci SQLException překladu.
  
[remark]:<slide>(new)
## Konfigurace 

**Gradle**
```groovy
dependencies {
    compile("org.springframework.boot:spring-boot-starter")
    compile("org.springframework:spring-jdbc")
    compile("com.h2database:h2")
    testCompile("junit:junit")
}
```

[remark]:<slide>(wait)
**Aplication properties**
```yaml
spring:
  h2:
    console:
      enabled: true
      
  datasource:
    url: jdbc:h2:file:./db/main-database;DB_CLOSE_ON_EXIT=FALSE
    username: test
    password: test
    driverClassName: org.h2.Driver    
```

[remark]:<slide>(new)
### Konfigurace pomocí `@Bean`
Pro konfihuraci zdrojem můžeme použít i Java definici

- příklad použijeme databázi MySQL):

```java
@Configuration
public class SpringJdbcConfig {
    @Bean
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/springjdbc");
        dataSource.setUsername("guest_user");
        dataSource.setPassword("guest_password");
 
        return dataSource;
    }
}
```

[remark]:<slide>(new)
### Konfigurace Embeded H2
Alternativně můžeme také dobře využít H2 databázi pro vývoj nebo testování.
 
Konfigurace H2 databáze a její naplění pomocí SQL skriptu:

```java
@Bean
public DataSource dataSource() {
    return new EmbeddedDatabaseBuilder()
               .setType(EmbeddedDatabaseType.H2)
               .addScript("classpath:jdbc/schema.sql")
               .addScript("classpath:jdbc/test-data.sql")
               .build();
}
```

[remark]:<slide>(new)
### Konfigurace pomocí XML
Jednoduchá konfigurace pomocí XML

```xml
<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"
  destroy-method="close">
    <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
    <property name="url" value="jdbc:mysql://localhost:3306/springjdbc"/>
    <property name="username" value="guest_user"/>
    <property name="password" value="guest_password"/>
</bean>
```

[remark]:<slide>(new)
## Použití 
Spring poskytuje třídu `JdbcTemplate`, která usnadňuje práci s relačními databázemi SQL a JDBC.

`JdbcTemplate` je rozhraní API, jehož prostřednictvím můžeme:

- vytvoření a uzavření spojení
- provádění příkazů a volání uložených procedur
- iterace přes `ResultSet` a vracení výsledků

[remark]:<slide>(wait)
#### Pomocí @Autowired
```java
@Autowired
private JdbcTemplate jdbcTemplate;
```

[remark]:<slide>(wait)
#### Použití `DataSource`
```java
private JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
```

[remark]:<slide>(new)
### Simple query
Jednoduchý příklad pro zíslání hodnoty z DB:

```java
int result = jdbcTemplate.queryForObject(
    "SELECT COUNT(*) FROM EMPLOYEE", Integer.class);
```

[remark]:<slide>(wait)
### Simple insert
Jednoduchý příklad pro INSERT:

```java
public int addEmplyee(int id) {
    return jdbcTemplate.update(
        "INSERT INTO EMPLOYEE VALUES (?, ?, ?, ?)", id, "Bill", "Gates", "USA");
}
```

V příkladě se používají ? pro nastavení parametru do příkazu.

Parametry se předávají podle pořadí.

[remark]:<slide>(new)
### Pojmenovanými parametry
Třída `NamedParameterJdbcTemplate` umožňuje pojmenovat paramtery v SQL dotazu.

Poskutuje ale i tradiční syntaxi pomocí `?` pro určení parametrů.

Implementace je provedena tak, že převod mezi `?` se provádí v této tříde.

```java
public String findFirstName(int id) {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
                                             .addValue("id", 1);
    
    return namedParameterJdbcTemplate.queryForObject(
        "SELECT FIRST_NAME FROM EMPLOYEE WHERE ID = :id", 
        namedParameters, 
        String.class);
}
```

[remark]:<slide>(new)
#### Třída `MapSqlParameterSource` 
Třída `MapSqlParameterSource` poskytuje hodnoty pro pojmenované parametry.

Implementací tohoto rohraní můžeme npaříklad automaticky záskat parametry z POJO objektu:

```java
public int countEmployeesWithFirstName(String firstName) {
    Employee employee = new Employee();
    employee.setFirstName("James");
     
    SqlParameterSource namedParameters;
    namedParameters = new BeanPropertySqlParameterSource(employee);
    
    return namedParameterJdbcTemplate.queryForObject(
            "SELECT COUNT(*) FROM EMPLOYEE WHERE FIRST_NAME = :firstName", 
            namedParameters, 
            Integer.class);
}
```

Do dotazu místo hodnot vkládáme implementace `BeanPropertySqlParameterSource` která parametry doplní pomocí jmen getteru.

[remark]:<slide>(new)
### Mapování výsledků dotazu do objektu
Další velmi užitečnou vlastností je schopnost mapovat výsledky dotazů na objekty Java.
 
K tomu je potřebná implementací rozhraní `RowMapper`.

Například - pro každý řádek vrácený dotazem použije jaro mapovač řádků k naplnění fazole java:

```java
public class EmployeeRowMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        Employee employee = new Employee();
 
        employee.setId(rs.getInt("ID"));
        employee.setFirstName(rs.getString("FIRST_NAME"));
        employee.setLastName(rs.getString("LAST_NAME"));
        employee.setAddress(rs.getString("ADDRESS"));
 
        return employee;
    }
}
```

[remark]:<slide>(new)
### Použití `RowMapper`
Následně můžeme použít `RowMapper` do dotazupro  získat plně naplněné objekty jazyka Java:

```java
public List<Employee> findEmployeesById(int id) {
    return jdbcTemplate.queryForObject(
            "SELECT * FROM EMPLOYEE WHERE ID = ?",
            new Object[] { id }, 
            new EmployeeRowMapper());
}
```

[remark]:<slide>(new)
## Překlad výjimek
Spring definuje hierarchií datových výjimek, se polčným předkem `DataAccessException`

Všechny ostatní výjimky zabaluje do této výjimky.
   
Spring takto odstinuje uživatele od nízko-urovňových výjimek.
    
také udržuje mechanismus zpracování výjimek nezávislý na databázi, kterou používáme.
    
Spring umožňje implementovat vlastní reakci na výjimky pomocí [SQLExceptionTranslator](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/jdbc/support/SQLExceptionTranslator.html).
 
[remark]:<slide>(new)
## Operace JDBC pomocí tříd `SimpleJdbc`
Třídy `SimpleJdbc` poskytují jednoduchý způsob práce s SQL příkazy. 

Tyto třídy používají metadata databáze k vytváření základních dotazů. 

Třídy `SimpleJdbcInsert` a `SimpleJdbcCall`  poskytují snadnější způsob volání vložených a uložených procedur.
      
[remark]:<slide>(wait)
### `SimpleJdbcInsert`

Podívejme se na provedení jednoduchých příkazů s minimální konfigurací.

Příkaz INSERT je generován na základě konfigurace `SimpleJdbcInsert` a vše, co potřebujeme, je poskytnout název tabulky, názvy sloupců a hodnoty.

```java
SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                                        .withTableName("EMPLOYEE");
```

[remark]:<slide>(new)
#### Použití `SimpleJdbcInsert`
Dále vytvoříme mapu s názvy sloupců a daty.
 
```java
public int addEmplyee(Employee emp) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("ID",         emp.getId());
    parameters.put("FIRST_NAME", emp.getFirstName());
    parameters.put("LAST_NAME",  emp.getLastName());
    parameters.put("ADDRESS",    emp.getAddress());
 
    return simpleJdbcInsert.execute(parameters);
}
```

[remark]:<slide>(wait)
Abychom umožnili databázi generovat primární klíč, můžeme využít `executeAndReturnKey()`,

```java
SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                                        .withTableName("EMPLOYEE")
                                        .usingGeneratedKeyColumns("ID");
 
Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
System.out.println("Generated id - " + id.longValue());
```

[remark]:<slide>(wait)
Nakonec můžeme tato data předat také pomocí `BeanPropertySqlParameterSource` a `MapSqlParameterSource`.

[remark]:<slide>(new)
### Uložené procedury s `SimpleJdbcCall`
`SimpleJdbcCall` umožňuje volání uložených procedůr.

```java
SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                    .withProcedureName("READ_EMPLOYEE");
```

```java
public Employee getEmployeeUsingSimpleJdbcCall(int id) {
    SqlParameterSource in = new MapSqlParameterSource()
                                .addValue("in_id", id);
    
    Map<String, Object> out = simpleJdbcCall.execute(in);
 
    Employee emp = new Employee();
    emp.setFirstName((String) out.get("FIRST_NAME"));
    emp.setLastName( (String) out.get("LAST_NAME"));
 
    return emp;
}
```

[remark]:<slide>(new)
## Dávkové operace
Dávkové operace porvádíme pomocí metody `batchUpdate()`.

Příklad dávky pomocí rozhraní `BatchPreparedStatementSetter`:
```java
public int[] batchUpdateUsingJdbcTemplate(List<Employee> employees) {
    return jdbcTemplate.batchUpdate("INSERT INTO EMPLOYEE VALUES (?, ?, ?, ?)",
        new BatchPreparedStatementSetter() {
        
            @Override
            public void setValues(PreparedStatement ps, int i) {
                ps.setInt(   1, employees.get(i).getId());
                ps.setString(2, employees.get(i).getFirstName());
                ps.setString(3, employees.get(i).getLastName());
                ps.setString(4, employees.get(i).getAddress();
            }
            
            @Override
            public int getBatchSize() {
                return employees.size();
            }
        });
}
```

[remark]:<slide>(new)
### Dávkové operace pomocí `NamedParameterJdbcTemplate`;

Máme také možnost dávkových operací s `NamedParameterJdbcTemplate.batchUpdate()`.

Tento způsob je jednodušší než předchozí - není třeba implementovat žádná další rozhraní pro nastavení parametrů.

Používá jména paramterů SQL pro sestavení dotazu.

```java
public int[] batchUpdateUsingJdbcTemplate(List<Employee> employees) {
    SqlParameterSource[] batch = SqlParameterSourceUtils
                                 .createBatch(employees.toArray());
    
    return updateCounts = namedParameterJdbcTemplate.batchUpdate(
        "INSERT INTO EMPLOYEE VALUES (:id, :firstName, :lastName, :address)", 
        batch);
}
```

[remark]:<slide>(new)
### Best practices
Snažte se databáze načítat co nejmenší množství objektů.

[remark]:<slide>(wait)
Pokud pracujete se polem (Listem) položek, vždy zvažte implementaci stránkování.

[remark]:<slide>(wait)
Z dotazů nikdy nevracejte `null`
- Použijte empty collections
- Iterátor 
- Optional