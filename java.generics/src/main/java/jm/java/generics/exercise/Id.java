package jm.java.generics.exercise;

import java.util.Objects;

public class Id<T> {

    private long value;
    private Class<T> type;

    public Id(long value, Class<T> type) {
        this.value = value;
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Id<?> id = (Id<?>) o;
        return value == id.value &&
                Objects.equals(type, id.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, type);
    }
}
