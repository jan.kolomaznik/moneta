package jm.java.generics.lesson;

import java.util.Collection;

public class Main {

    public static void main(String[] args) {

    }

    public static double suma(Collection<Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

}
