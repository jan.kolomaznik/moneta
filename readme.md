SPRING FRAMEWORK - ZÁKLADNÍ KURZ
================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

V rámci základního školení se seznámíte s možnostmi vývoje aplikací v prostředí Spring Framework. 
Kurz Vás dále seznámí s možnostmi využití servletu, JavaServer Pages (JSP), Spring MVC, práci s databází, apod.

Osnova kurzu 
------------
- **[Úvod do Spring Framework](/topic/spring)**
  - Architektura Spring Framework
  - Spring & Java Beans
  - Konfigurace aplikace a metadat
- **[Spring Boot](/topic/spring.boot) - samostatné aplikace**
- **[Dependency injection](/topic/spring.di)**
- **[Spring MVC](/topic/spring.boot.mvc)**
  - Definice
  - Vztah k standardní Javě
  - Vytvoření základní MVC aplikace
  - Vytvoření základního kontroleru a pohledu v JSP
- **[View technologie](/topic/spring.boot.mvc-thymeleaf) (Thymeleaf)**
  - Jednoduchá strana
  - Knihovna značek
- **[Servlet](/topic/spring.jee)**
  - Definice a vytvoření
  - Generování výstupu
  - Zpracování vstupu a výstupu (jednoduchá formulářová aplikace)
- **Spring Rest**
  - [RESTful Web Services](/topic/spring.boot.rest-service)
  - [Hypermedia REST](/topic/spring.boot.rest-hypermedia)
- **Přístup k datům a integrace s MVC**
  - Spring Data
  - JDBC (bez hibernate)
  - Data a REST
- **[Aspektově-orientované programování (AOP) v Springu](/topic/spring.boot.aop)**
- **[Konfigurace aplikace](/topic/spring.boot.config)**

SPRING FRAMEWORK - PRO POKROČILÉ
================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

V rámci tohoto školení se seznámíte s pokročilými možnostmi vývoje J2EE aplikací v prostředí Spring Framework. Kurz Vás seznámí s možnostmi využití Maven, Java Persistence API (JPA), Java Persistence Query Language (JPQL) a prací ve Spring MVC. 

Osnova kurzu 
------------

- **[Zhrnutí základního kurzu](/topic/spring)**
   - Co je to Spring, základní přehled komponent
   - Spring Boot a Spring Initializr
   - Magické zkratky IoC a DI
   - Základní pojmy
- **[Spring context](/topic/spring.context)**
- **[Maven/Gradle](/topic)**
- **[Java Persistence API (JPA)](/topic/spring.)**
  - koncept entitních tříd
  - perzistentní jednotky
  - API entitního správce
  - aplikace Hibernate jako JPA poskytovatele
  - Java Persistence Query Language (JPQL)
- **[Transakce ve Springu a řízení z aplikace](/topic/spring.)**
- **~~Validace na úrovni entitních třid podle JSR 303~~**
- **~~Koncept lokalizace ve Spring MVC~~**
- **[Spring MVC](/topic/spring.)**
- **[Pretty URL mapping v MVC](/topic/spring.)**
- **[Ladění aplikací](/topic/spring.)**
    
