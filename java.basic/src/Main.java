import java.util.Objects;

public class Main {

    private static final String konstatnta = "NECO";
    
    public static void main(String[] args) {
        String str1 = "ABC";
        String str2 = new String("ABC");

        System.out.printf("%s == %s : %s\n", str1, str2, str1 == str2);
        System.out.printf("%s equels %s : %s\n", str1, str2, Objects.equals(str1, str2));

        Integer int1 = 1000;
        Integer int2 = 1000;

        System.out.printf("%d == %d : %s\n", int1, int2, int1 == int2);
        System.out.printf("%d equels %d : %s\n", int1, int2, Objects.equals(int1, int2));
    }
}
