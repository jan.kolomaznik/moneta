package cz.moneta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericDemo {

    public static double suma(List<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> cisla = Arrays.asList(1,2,3,4);
        //ArrayList<Number> arrayList = new ArrayList<>(cisla);
        System.out.println(suma(cisla));
    }
}
