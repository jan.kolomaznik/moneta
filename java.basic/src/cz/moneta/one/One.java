package cz.moneta.one;

/**
 *
 */
public abstract class One {

    protected int number = 5;

    /**
     *
     * @return
     */
    public int getNumber(int mutiple) {
        return number * mutiple;
    }

    /**
     *
     * @param number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    public abstract void fooo();
}
