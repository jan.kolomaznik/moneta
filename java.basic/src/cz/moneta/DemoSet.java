package cz.moneta;

import java.util.HashSet;
import java.util.Set;

public class DemoSet {

    public static void main(String[] args) {
        Coordinate p1 = new Point(1,2);
        Point p2 = new Point(3,2);

        Set<Coordinate> set = new HashSet<>();
        set.add(p1);
        set.add(p2);
        System.out.println(set.toString());

        p2.setX(1);
        System.out.println(set.toString());


        set.remove(new Point(1,2));
        System.out.println(set.toString());

    }
}
