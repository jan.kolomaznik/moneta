package cz.moneta;

import static java.lang.Math.abs;

public interface Coordinate<T> {

    T getX();
    T getY();

    /*
    default double distanceTo(Coordinate other) {
        return abs(getX() - other.getX()) + abs(getY() - other.getY());
    }*/

}
