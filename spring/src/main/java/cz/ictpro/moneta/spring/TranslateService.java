package cz.ictpro.moneta.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class TranslateService {

    public String translate(String msg) {
        return msg.toUpperCase();
    }
}
