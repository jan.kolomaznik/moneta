package cz.ictpro.moneta.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

@ShellComponent
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private ApplicationContext context;

    @Autowired
    private TranslateService ts;

    @ShellMethod("Hello form shell.")
    public String hello(String name) {
        String translate = ts.translate(name);
        return format("Hello %s%n", translate);
    }

    @ShellMethod("All beans.")
    public String allBeans() {
        return Stream.of(context.getBeanDefinitionNames())
                .collect(Collectors.joining("\n"));
    }

    @ShellMethod("Get bean by name.")
    public String bean(String name) {
        return context.getBean(name).toString();
    }

    @Autowired
    private MessageService enMessageService;

    @Autowired
    private MessageService czMessageService;

    @Autowired
    private Optional<OptionServiceDemo> optionServiceDemo;

    @ShellMethod("Get bean by config.")
    public void config() {
        System.out.printf("SK Message service == %s%n", optionServiceDemo);
        System.out.printf("Message service == %s%n", (enMessageService == czMessageService));
        System.out.printf("Translate service == %s%n",
                (enMessageService.getTranslateService() == czMessageService.getTranslateService()));
    }

}
