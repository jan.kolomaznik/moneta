package cz.ictpro.moneta.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaConfig {

    @Bean
    @Scope("prototype")
    public TranslateService translateService() {
        return new TranslateService();
    }

    @Bean
    public MessageService enMessageService() {
        return new MessageService(translateService());
    }

    @Bean
    public MessageService czMessageService() {
        return new MessageService(translateService());
    }
}
