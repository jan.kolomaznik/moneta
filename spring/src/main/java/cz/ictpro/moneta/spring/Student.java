package cz.ictpro.moneta.spring;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
public class Student {

    private String name;

    private String surName;

    private int age;

    private String status;

    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student);
    }

}
