package cz.ictpro.moneta.spring;

public class MessageService {

    private TranslateService translateService;

    public MessageService(TranslateService translateService) {
        this.translateService = translateService;
    }

    public TranslateService getTranslateService() {
        return translateService;
    }
}
